# terraform-module-demo-root

This is a terraform root module, which exists to demonstrate how to extract a
child module from a root module.


## Structure

In the initial version, this repo contains the following files:
```
README.md  # this readme
LICENSE  # MIT license
servers.tf  # terraform file for "servers"
variables.tf  # input variables
```

Note that the file called `servers.tf` only creates local text files under
`/tmp`, because this allows the demo to run locally without actually needing
cloud resources, an existing k8s cluster, or any other friction-inducing
requirements; if you have `git`, `terraform`, and the ability to clone things
from gitlab, this demo should work.

We will later replace `servers.tf` with `main.tf`, which will call a child
module to create 'servers' for us.


## Prerequisites

As noted above, you will need `git`, `terraform`, and the ability to clone from
gitlab to run this demo.

This repo was written and tested with terraform 1.2.5; if you have tfenv
installed then you can proceed (I've included `.terraform-version`); if not, I
strongly suggest downloading/installing terraform 1.2.x to ensure compatibility.


## Use

First, clone this repo somewhere convenient, and `cd` into it.

Then:
```
git checkout 1
terraform init
terraform plan
terraform apply
find /tmp/terraform-module-demo-*
```

This should always produce a result like the following:
```
$ find /tmp/terraform-module-demo-*
/tmp/terraform-module-demo-staging
/tmp/terraform-module-demo-staging/frontend-2
/tmp/terraform-module-demo-staging/frontend-1
/tmp/terraform-module-demo-staging/backend-2
/tmp/terraform-module-demo-staging/backend-1
/tmp/terraform-module-demo-var.environment_name
```

Now to upgrade in place:)
```
git checkout 2
terraform init
terraform state mv local_file.server_frontend_1 module.demo.local_file.server_frontend_1
terraform state mv local_file.server_frontend_2 module.demo.local_file.server_frontend_2
terraform state mv local_file.server_backend_1 module.demo.local_file.server_backend_1
terraform state mv local_file.server_backend_2 module.demo.local_file.server_backend_2
terraform plan  # this should show no changes
find /tmp/terraform-module-demo-*
```

At this point we're done; this terraform stack is now switched to the module.
However, there's not much point in creating a child module if you're only using
it once. Let's create multiple environments with it. Picking up where we left
off:
```
git checkout 3
ls *  # observe the new layout: the previous stack has been moved to a subdirectory "staging", and a "production" directory has been added.
mv terraform.tfstate .terraform .terraform.lock.hcl staging/  # since git doesn't track it, move state over manually
cd staging
terraform plan
terraform apply  # actually create the new output
cd ../production
terraform init
terraform plan
terraform apply
find /tmp/terraform-module-demo-*
```

And now, we can do something *really* fun:) Once again, beginning from the root
of the repo, after executing the previous steps (or in a pinch, `git checkout 3
&& cd staging && terraform init && terraform apply` should work):
```
git checkout 4
cd staging
cat main.tf  # we are now using module version 1.2.0
terraform init
terraform apply
```
This upgrades you to use version 1.2.0 of the module, which changed from
statically declaring 2 frontend and 2 backend servers to declaring an arbitrary
count of frontend and backend servers (still defaulting to 2 of each), and while
you do have to actually run `terraform apply` to do it, the module can even
include automatic migration steps (which is what you just did), even though the
resources changed from individual resources to single per-type resources with a
`count` applied! You can see the actual code to do this at
https://gitlab.com/yjftsjthsd-g/terraform-module-demo-module/-/blob/2179a58efdf67a9f5b2b689015db0d783f95355a/servers.tf
if you're curious.

This is also a good time to highlight a benefit of structuring the repo this
way, and breaking out the "application environment" into a module: The staging
environment is now on version 1.2.0, but production is still on 1.1.0; this
decoupling allows you to easily propagate changes through your change control
process, one environment at a time. 

To clean up (starting in the root of the checkout):
```
cd staging
terraform destroy
cd ../production
terraform destroy
rmdir /tmp/terraform-module-demo-*
```

## License

This repo is available under the MIT license; see LICENSE for details.

